from tika import parser
from io import BytesIO
import pdftotext
from docx import Document
from urllib import request
import pytesseract as pt
import pdf2image
from bidi import algorithm


class ResumeParser:
    def __init__(self, path, doc_type, parser_type, lang):
        self.path = path
        self.doc_type = doc_type
        self.parser_type = parser_type
        self.lang = lang

    def parse_resume(self):
        if self.doc_type == 'pdf':
            if self.parser_type == 'one':
                return self.parse_pdf_one()
            if self.parser_type == 'two':
                return self.parse_pdf_two()
            if self.parser_type == 'three':
                return self.parse_pdf_three()
        else:
            if self.parser_type == 'one':
                return self.parse_word_one()

    def parse_pdf_one(self):
        contents = ''
        f = request.urlopen(self.path).read()
        fp = BytesIO(f)
        pdf = pdftotext.PDF(fp)
        for page in pdf:
            contents = ' '.join([contents, page])
        if self.lang == 'eng':
            return contents
        else:
            return algorithm.get_display(contents)

    def parse_pdf_two(self):
        file_data = parser.from_file(self.path)
        contents = file_data['content']
        contents = contents.split('\n')
        contents = [line for line in contents if line.strip() != '']
        if self.lang == 'eng':
            return '\n'.join(contents)
        else:
            return algorithm.get_display('\n'.join(contents))

    def parse_pdf_three(self):
        f = request.urlopen(self.path).read()
        pages = pdf2image.convert_from_bytes(f, dpi=200, size=(1654, 2340))
        content = ''
        for page in pages:
            content = '\n'.join([pt.image_to_string(page, lang=self.lang), content])
        return content

    def parse_word_one(self):
        f = request.urlopen(self.path).read()
        fp = BytesIO(f)
        doc = Document(fp)
        contents = ''
        for paragraph in doc.paragraphs:
            contents = '\n'.join([contents, paragraph.text])
        return contents
