from flask import Flask
from flaskext.mysql import MySQL
from flask_cors import CORS
from flask_cors import cross_origin
from flask import request
import json
import analyzer
import parser
import keywords

# print a nice greeting.
def say_hello(username = "World"):
    return '<p>Hello %s!</p>\n' % username

# some bits of text for the page.
header_text = '''
    <html>\n<head> <title>EB Flask Test</title> </head>\n<body>'''
instructions = '''
    <p><em>Hint</em>: This is a RESTful web service! Append a username
    to the URL (for example: <code>/Thelonious</code>) to say hello to
    someone specific.</p>\n'''
home_link = '<p><a href="/">Back</a></p>\n'
footer_text = '</body>\n</html>'

# EB looks for an 'application' callable by default.
application = Flask(__name__)
CORS(application, resources={r"/*": {"origins": "*"}})
mysql = MySQL()
application.config['MYSQL_DATABASE_USER'] = 'parser'
application.config['MYSQL_DATABASE_PASSWORD'] = 'Career12345'
application.config['MYSQL_DATABASE_DB'] = 'courses'
application.config['MYSQL_DATABASE_HOST'] = '52.29.179.35'
mysql.init_app(application)

# add a rule for the index page.
application.add_url_rule('/', 'index', (lambda: header_text +
    say_hello() + instructions + footer_text))

# add a rule when the page is accessed with a name appended to the site
# URL.
application.add_url_rule('/<username>', 'hello', (lambda username:
    header_text + say_hello(username) + home_link + footer_text))


@application.route('/api/feed/data', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
def feed():
    filter_groups = request.args.getlist('groups')
    filter_areas = request.args.getlist('areas')
    filter_specialisations = request.args.getlist('specialisations')
    limit = request.args.get('limit')
    conn = mysql.connect()
    cursor = conn.cursor()
    specialisations_clause = "group_specialisation IN ('" + "','".join(filter_specialisations) + "')"
    areas_clause = "group_area IN ('" + "','".join(filter_areas) + "')"
    groups_clause = "group_name NOT IN ('" + "','".join(filter_groups) + "')"
    if len(filter_groups) > 0 or len(filter_areas) > 0 or len(filter_specialisations) > 0:
        clauses = []
        if len(filter_groups) > 0:
            clauses.append(groups_clause)
        if len(filter_specialisations) > 0:
            clauses.append(specialisations_clause)
        if len(filter_areas) > 0:
            clauses.append(areas_clause)
        clause = " AND ".join(clauses)
        cursor.execute("SELECT * from facebook_groups WHERE " + clause + " ORDER BY STR_TO_DATE(posted_time, '%W, %M %d, %Y at %H:%i') DESC LIMIT " + limit)
    else:
        cursor.execute("SELECT * from facebook_groups ORDER BY STR_TO_DATE(posted_time, '%W, %M %d, %Y at %H:%i') DESC LIMIT " + limit)
    row_headers = [x[0] for x in cursor.description]  # this will extract row headers
    rv = cursor.fetchall()
    json_data = []
    for result in rv:
        json_data.append(dict(zip(row_headers, result)))
    return json.dumps(json_data)


@application.route('/api/feed/groups', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
def groups():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT DISTINCT group_name, group_area, group_specialisation FROM facebook_groups WHERE group_specialisation IS NOT NULL")
    row_headers = [x[0] for x in cursor.description]  # this will extract row headers
    rv = cursor.fetchall()
    json_data = []
    for result in rv:
        json_data.append(dict(zip(row_headers, result)))
    return json.dumps(json_data)


@application.route('/api/feed/specialisations', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
def specialisations():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT DISTINCT group_specialisation FROM facebook_groups WHERE group_specialisation IS NOT NULL")
    row_headers = [x[0] for x in cursor.description]  # this will extract row headers
    rv = cursor.fetchall()
    json_data = []
    for result in rv:
        json_data.append(result[0])
    return json.dumps(json_data)


@application.route('/api/feed/areas', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
def areas():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT DISTINCT group_area FROM facebook_groups WHERE group_area IS NOT NULL")
    row_headers = [x[0] for x in cursor.description]  # this will extract row headers
    rv = cursor.fetchall()
    json_data = []
    for result in rv:
        json_data.append(result[0])
    return json.dumps(json_data)


@application.route('/api/relevant', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
def relevant_resume():
    return json.dumps({'is_relevant': 'true'})


@application.route('/api/relevant-tes', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
def parse_resume():
    return json.dumps({'is_relevant': 'true'})


@application.route('/api/parser', methods=['GET'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
def resume_parser():
    resume_extractor = parser.ResumeParser(request.args['path'], request.args['doc_type'], request.args['parser'], request.args['lang'])
    return resume_extractor.parse_resume()


@application.route('/api/keywords-inspector', methods=['POST'])
@cross_origin(origin='*', headers=['Content-Type', 'application/json'])
def keywords_parser():
    print(request.get_json())
    resume_inspector = keywords.KeywordsExtractor(request.get_json()['job_description'], request.get_json()['cv'])
    keywords_list = resume_inspector.makeTable()
    similarity = resume_inspector.printMeasures()
    return json.dumps({'keywords': keywords_list, 'similarity': similarity})


if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    application.debug = True
    application.run()
