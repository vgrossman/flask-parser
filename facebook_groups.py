import requests
from bs4 import BeautifulSoup, Comment
import xlrd
import pymysql


def db_connect(post_details):

    db = pymysql.connect("52.29.179.35", "parser", "Career12345", "courses", 3306)
    cursor = db.cursor()

    sql = "INSERT INTO facebook_groups (`post_id`, `posted_name`, `posted_time`, `posted_photo`, `text`, `photo`, " \
          "`group_name`, `group_link`, `group_area`, `group_specialisation`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % \
          (post_details['post_id'], post_details['name'], post_details['timestamp'], post_details['img_src'],
           db.escape_string(post_details['post_content']), post_details['post_image_src'], post_details['group_name'],
           post_details['group_link'], post_details['group_area'], post_details['group_specialisation'])
    try:
        # Execute the SQL command
        cursor.execute(sql)
        # Commit your changes in the database
        db.commit()
    except Exception as e:
        # Rollback in case there is any error
        print(e)
        db.rollback()
    db.close()


def get_all_posts(url, group_name, group_area, group_specialisation):
    post_details = {}
    group_id = url.split('groups/')
    group_id = 'group_mall_' + group_id[1]
    headers = {"Accept-Language": "en-US,en;q=0.5"}
    r = requests.get(url=url, headers=headers)
    soup = BeautifulSoup(r.content, 'html5lib')
    for comments in soup.findAll(text=lambda text: isinstance(text, Comment)):
        comment_soup = BeautifulSoup(comments.extract(), 'html5lib')
        data_object = comment_soup.findAll('div', id=group_id)
        for single_data in data_object:
            articles_object = single_data.select("div[id^=mall_post_]")
            for single_article in articles_object:
                post_details['post_id'] = single_article.get('id')
                img_object = single_article.find('img', class_='img')
                if img_object:
                    post_details['name'] = img_object.get('aria-label').replace("'", "")
                    post_details['img_src'] = img_object.get('src')
                    timestamp = single_article.find('abbr')
                    post_details['timestamp'] = timestamp.get('title')
                    post_content = single_article.find('div', {'data-testid': 'post_message'})
                    post_image = single_article.find('img', class_='scaledImageFitWidth')
                    if (not post_content) and (not post_image):
                        continue
                    if post_image:
                        post_details['post_image_src'] = post_image.get('src')
                    else:
                        post_details['post_image_src'] = ""

                    if post_content:
                        for span in post_content.find_all("span", {'class': 'text_exposed_hide'}):
                            span.decompose()
                        for span in post_content.find_all("span", {'class': '_5mfr'}):
                            span.decompose()
                        for tag in post_content.find_all():
                            for attr in list(tag.attrs):
                                if attr != 'href':
                                    del tag.attrs[attr]
                        post_details['post_content'] = str(post_content)
                        # '\n'.join(post_content.findAll(text=True)).replace("'", "")
                    else:
                        post_details['post_content'] = ""

                    post_details['group_link'] = url
                    post_details['group_area'] = group_area
                    post_details['group_specialisation'] = group_specialisation
                    post_details['group_name'] = group_name.replace("'", "")

                    db_connect(post_details)


def get_data_from_csv():
    loc = "groups.xlsx"
    wb = xlrd.open_workbook(loc)
    sheet = wb.sheet_by_index(0)
    for i in range(sheet.nrows):
        if i > 0:
            group_name = sheet.cell_value(i, 0)
            group_url = sheet.cell_value(i, 3)
            group_area = sheet.cell_value(i, 1)
            group_specialisation = sheet.cell_value(i, 2)
            get_all_posts(group_url, group_name, group_area, group_specialisation)


get_data_from_csv()
