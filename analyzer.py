from googletrans import Translator
from tika import parser
from io import BytesIO
import spacy
import pdftotext
import re
import string
import regex
from docx import Document
from textblob import TextBlob
from openpyxl import load_workbook
from urllib import request
from spacy.lang.en import STOP_WORDS


sections = load_workbook(filename="./keywords/sections.xlsx")
skills = load_workbook(filename="./keywords/skills.xlsx")
universities = load_workbook(filename="./keywords/universities.xlsx")
keywords = load_workbook(filename="./keywords/keywords_new.xlsx")
keywords_extractor = load_workbook(filename="./keywords/keywords_new.xlsx")

vacancy = {
    'vacancy_name': '',
    'vacancy_description': 'Landa Labs is seeking a talented IT support to join our IT department. In this position your job responsibilities will include the following main tasks:Provides service desk support to resolve complex technical hardware and software problems.Monitor and respond quickly and effectively to requests received through the IT Service Desk ticket System.Requirements:At least 3 years of experience as a technician in a similar organizationAt least 3 years of experience with Active Directory accounts and permissionsExperience in hardware, software, and network troubleshootingExperience with Windows 7 and Windows 10 Operating Systems, including diagnosing and correcting issuesExperience with Installing computers and laptops from scratchExperience with VDIExperience with Microsoft Office O365 suiteHands on experience with mobile devices (IOS and Android)Ability to provide Off-hours support as neededParticipation in on call duty from time to timeMust have a very good English in order to provide effective phone, desk-side, and email supportBeing organized individual, attention to detailsPro-active, care for users satisfaction, team workerAbility to write technical documents and procedures',
    'hard_skills': '',
    'soft_skills': '',
    'job_functions': '',
    'specialisations': '',
    'location': '',
}

def extract_email(text):
    try:
        match = re.search(r'[\w\.-]+@[\w\.-]+', text)
        return match.group(0)
    except:
        return ''


def extract_phone(text):
    try:
        match = re.search(
            r'(?:(?:\+?([0-9]|[0-9][0-9]|[0-9][0-9][0-9])\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([0-9][1-9]|[0-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?',
            text)
        return match.group(0)
    except:
        return ''


def extract_linkedin(text):
    try:
        match = re.search(r'(\S*linkedin\S*)', text)
        return match.group(0)
    except:
        return ''


def extract_github(text):
    try:
        match = re.search(r'(\S*github\S*)', text)
        return match.group(0)
    except:
        return ''


def extract_dribble(text):
    try:
        match = re.search(r'(\S*dribble\S*)', text)
        return match.group(0)
    except:
        return ''


def extract_behance(text):
    try:
        match = re.search(r'(\S*github\S*)', text)
        return match.group(0)
    except:
        return ''


def recover_sentences(dictlist):
    sentences = []
    for _line in dictlist:
        if _line.rstrip().lstrip().split(' ')[-1] in STOP_WORDS or _line.rstrip().lstrip().endswith(','):
            sentences.append(_line.rstrip().lstrip())
        else:
            sentences.append(_line.rstrip().lstrip())
            yield " ".join(sentences)
            sentences = []


def recover_skills(dictlist):
    sentences = []
    for _line in dictlist:
        if _line.rstrip().lstrip().split()[0] in ['-', '✓', '•']:
            if len(sentences) > 0:
                yield " ".join(sentences)
                sentences = []
            sentences.append(_line.rstrip().lstrip())
        else:
            sentences.append(_line.rstrip().lstrip())
            yield " ".join(sentences)
            sentences = []


def translate(to_translate):
    translator = Translator()
    result = translator.translate(to_translate, src='he', dest='en')
    return result.text


def clear_text(sent):
    sent = re.sub(u'[•]', '', sent)
    sent = re.sub(u'[✓]', '', sent)
    sent = re.sub(u'[.]', '', sent)
    sent = re.sub(u'[(|)]', '', sent)
    sent = re.sub(u'[(–)]', ' ', sent)
    sent = re.sub(u'[(,)]', '', sent)
    sent = re.sub(u'[(\-)]', ' ', sent)
    sent = re.sub(u'\u202a', ' ', sent)
    sent = re.sub(u'\u202b', ' ', sent)
    sent = re.sub(u'\u202c', ' ', sent)
    sent = re.sub('\d\.\s+|[a-z]\)\s+|•\s+|[A-Z]\.\s+|[IVX]+\.\s+/', '', sent)
    sent = ' '.join(sent.split('\n'))
    sent = re.split(r'\s{2,}', sent)
    sent = ' '.join(sent)
    # sent = re.split(r'\s{2,}', sent)
    # sent = ''.join(sent)
    return sent


def identify_language(text):
    b = TextBlob(text)
    return b.detect_language()


def extract_keywords(text):
    keywords_match = []
    for row in keywords.active:
        pattern = '(?:[.,!?\-\n\t]|\s)(%s)(?:[.,!?\-\n\t]|\s)' % regex.escape(row[0].value)
        if regex.search(pattern, text, regex.BESTMATCH):
            try:
                if row[1].value != 'Yes':
                    keywords_match.append(regex.search(pattern, text, regex.IGNORECASE | regex.BESTMATCH).group(1))
                else:
                    keywords_match.append(regex.search(pattern, text, regex.BESTMATCH).group(1))
            except:
                continue
    return keywords_match


def extract_skills(text):
    skills_match = []
    for row in skills.active:
        if row[3].value == 'No':
            continue
        pattern = '(?:[.,!?\-\n\t]|\s)(%s)(?:[.,!?\-\n\t]|\s)' % regex.escape(row[1].value)
        if regex.search(pattern, text, regex.BESTMATCH):
            try:
                if row[3].value != 'Yes':
                    skills_match.append(regex.search(pattern, text, regex.IGNORECASE | regex.BESTMATCH).group(1))
                else:
                    skills_match.append(regex.search(pattern, text, regex.BESTMATCH.group(1)))
            except:
                continue
        pattern = '(?:[.,!?\-\n\t]|\s)(%s)(?:[.,!?\-\n\t]|\s)' % regex.escape(row[1].value)
        if regex.search(pattern, text, regex.IGNORECASE, regex.IGNORECASE | regex.BESTMATCH):
            try:
                if row[3].value != 'Yes':
                    skills_match.append(regex.search(pattern, text, regex.IGNORECASE | regex.BESTMATCH).group(1))
                else:
                    skills_match.append(regex.search(pattern, text, regex.BESTMATCH).group(1))
            except:
                continue
    return skills_match


class ResumeAnalysis:
    def __init__(self, path, job_functions, specialisations, hard_skills, languages):
        self.path = path
        self.job_functions_required = list(map(lambda x: x['id'], job_functions))
        self.specialisations_required = list(map(lambda x: x['id'], specialisations))
        self.hard_skills_required = list(map(lambda x: x['id'], hard_skills))
        self.languages_required = list(map(lambda x: x['id'], languages))
        self.candidate_text = ""
        self.candidate_text_translated = ""
        self.data = []
        self.doc_lang = ''
        self.vacancy_json = {
            'skills': '',
            'keywords': '',
        }
        self.sections_data = {}
        self.candidate_json = {
            'email': '',
            'phone': '',
            'linkedin': '',
            'github': '',
            'behance': '',
            'dribble': '',
            'hard_skills': '',
            'keywords': '',
            'education': '',
            'experience': '',
            'other': '',
        }

    def extract_educations_section(self):
        educations = {'0': []}
        i = 0
        for line in self.sections_data['education']:
            if re.search('[1-3][0-9]{3}', line):
                i = i + 1
                educations[str(i)] = [line]
            else:
                educations[str(i)].append(line)

    def extract_work_section(self):
        work = {'0': []}
        i = 0
        for line in self.sections_data['work experience']:
            if re.search(r'[1-3][0-9]{3}', line):
                i = i + 1
                work[str(i)] = [line]
            else:
                work[str(i)].append(line)

    def extract_languages_section(self):
        languages = {}
        languages_text = ' '.join(self.sections_data['languages']).translate(str.maketrans('', '', string.punctuation))
        languages_text = languages_text.replace('–', '')
        languages_text = languages_text.replace('✓', '')
        lang = ''
        for word in languages_text.split(' '):
            word = word.rstrip().lstrip()
            if word != '':
                if word.lower() in ['english', 'hebrew', 'mandarin', 'russian', 'french']:
                    languages[word.lower()] = []
                    lang = word.lower()
                else:
                    languages[lang].append(word.lower())

    def extract_skills_section(self):
        recover_skills(self.sections_data['skills'])

    def get_all_sections(self):
        sections_names = []
        for row in sections.active:
            if row[1].value == self.doc_lang:
                sections_names.append((row[0].value, row[2].value))
        sections_data = {'header': []}
        for line in self.candidate_text.split('\n'):
            restart = False
            exclude = set(string.punctuation)
            cleaned_line = ''.join(ch for ch in line.lstrip().rstrip().lower() if ch not in exclude)
            cleaned_line = re.sub(u'\u202a', ' ', cleaned_line)
            cleaned_line = re.sub(u'\u202b', ' ', cleaned_line)
            cleaned_line = re.sub(u'\u202c', ' ', cleaned_line)
            cleaned_line = cleaned_line.rstrip().lstrip()
            for section in sections_names:
                if cleaned_line == section[0]:
                    sections_data[section[1]] = []
                    restart = True
                    break
            if restart:
                continue
            else:
              sections_data[list(sections_data.keys())[-1]].append(line.lstrip().rstrip())
        self.sections_data = sections_data


    def text_languages(self):
        try:
            languages = ['english', 'hebrew', 'anglit', 'ivrit']
            return languages
        except:
            return ''

    def extract_universities(self):
        universities_match = []
        for row in universities.active:
            if regex.search('((?:%s))' % row[1].value, self.candidate_text, regex.IGNORECASE):
                universities_match.append(regex.search('((?:%s){e<=1})' % row[1].value, self.candidate_text, regex.IGNORECASE | regex.BESTMATCH).group(0))
            if regex.search('((?:%s))' % row[2].value, self.candidate_text, regex.IGNORECASE):
                universities_match.append(regex.search('((?:%s){e<=1})' % row[2].value, self.candidate_text, regex.IGNORECASE | regex.BESTMATCH).group(0))
        return universities_match

    def extract_languages(self):
        return ''

    def extract_location(self):
        return ''

    def pdf_to_text(self):
        f = request.urlopen(self.path).read()
        fp = BytesIO(f)
        pdf = pdftotext.PDF(fp)
        sent = ''
        self.doc_lang = identify_language(pdf[0])
        if self.doc_lang == 'iw':
            for page in pdf:
                sent = ' '.join([sent, page])
            self.candidate_text_translated = translate(sent)
        else:
            file_data = parser.from_file(self.path)
            sent = file_data['content']
        return sent

    def doc_to_text(self):
        f = request.urlopen(self.path).read()
        fp = BytesIO(f)
        fulltext = []
        doc = Document(fp)
        for para in doc.paragraphs:
            for line in para.text.split('\n'):
                if line is not "" and " ":
                    fulltext.append(line.rstrip().lstrip())
        self.doc_lang = identify_language(" ".join(fulltext))
        return 0

    def extract_keywords_nlp(self):
        if '.pdf' in self.path[-5:]:
            self.candidate_text = self.pdf_to_text()
        elif '.doc' in self.path[-5:]:
            self.candidate_text = self.doc_to_text()
        if self.doc_lang == 'iw':
            return 0
        nlp = spacy.load("en_core_web_sm")
        text = nlp(clear_text(self.candidate_text))
        for nc in text.noun_chunks:
            result = [item for item in nc.text.split(' ') if item not in STOP_WORDS and ['eg', 'etc', 'e.g.', 'etc']]
            print(result)
        # for ent in text.ents:
        #    print(ent.text, ent.start_char, ent.end_char, ent.label_)

    def analyze_candidate(self):
        print(self.job_functions_required)
        print(self.specialisations_required)
        print(self.hard_skills_required)
        print(self.languages_required)
        if '.pdf' in self.path[-5:]:
            self.candidate_text = self.pdf_to_text()
        elif '.doc' in self.path[-5:]:
            self.candidate_text = self.doc_to_text()
        self.candidate_json['email'] = extract_email(self.candidate_text)
        self.candidate_json['phone'] = extract_phone(self.candidate_text)
        self.candidate_json['linkedin'] = extract_linkedin(self.candidate_text)
        self.candidate_json['github'] = extract_github(self.candidate_text)
        self.candidate_json['behance'] = extract_behance(self.candidate_text)
        self.candidate_json['dribble'] = extract_dribble(self.candidate_text)
        self.candidate_json['universities'] = self.extract_universities()
        if self.doc_lang == 'iw':
            self.candidate_json['hard_skills'] = extract_skills(self.candidate_text) + extract_skills(self.candidate_text_translated)
        else:
            self.candidate_json['hard_skills'] = extract_skills(self.candidate_text)
        if self.doc_lang == 'iw':
            self.candidate_json['keywords'] = extract_keywords(self.candidate_text) + extract_keywords(self.candidate_text_translated)
        else:
            self.candidate_json['keywords'] = extract_keywords(self.candidate_text)
        self.get_all_sections()
        extracted_sections = list(self.sections_data.keys())
        if 'work' in extracted_sections:
            print('extracting work')
            self.extract_work_section()
        if 'education' in extracted_sections:
            print('extracting education')
            self.extract_educations_section()
        if 'languages' in extracted_sections:
            print('extracting languages')
            self.extract_languages_section()
        if 'skills' in extracted_sections:
            print('extracting languages')
            self.extract_skills_section()
        print('Language: ', self.doc_lang)
        print('Email: ', self.candidate_json['email'])
        print('Phone: ', self.candidate_json['phone'])
        print('Skills: ', self.candidate_json['hard_skills'])
        print('Keywords: ', self.candidate_json['keywords'])
        print('Sections: ', self.sections_data)
        print(self.candidate_json)
        return self.candidate_json
