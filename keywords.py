import re
import pandas as pd
import sys, os
import numpy as np
from docx import Document
import nltk
import operator
import math
import parser
import pdftotext
from tika import parser
from urllib import request
from textblob import TextBlob
from io import BytesIO
import pytesseract as pt
import pdf2image


def identify_language(text):
    b = TextBlob(text)
    return b.detect_language()


class KeywordsExtractor:
    def __init__(self, job_description, cv_path):
        self.path = cv_path
        self.softskills = self.load_skills('softskills.txt')
        self.hardskills = self.load_skills('hardskills.txt')
        self.jb_distribution = self.build_ngram_distribution(job_description)
        self.cv_distribution = self.build_ngram_distribution(self.parse_cv())
        self.table = []
        self.outFile = "Extracted_keywords.csv"

    def parse_cv(self):
        if '.pdf' in self.path[-5:]:
            return self.pdf_to_text()
        elif '.doc' in self.path[-5:]:
            return self.doc_to_text()

    def pdf_to_text(self):
        f = request.urlopen(self.path).read()
        fp = BytesIO(f)
        pdf = pdftotext.PDF(fp)
        sent = ''
        if len(pdf[0]) > 3:
            if identify_language(pdf[0]) == 'iw':
                for page in pdf:
                    sent = ' '.join([sent, page])
            else:
                file_data = parser.from_file(self.path)
                contents = file_data['content']
                contents = contents.split('\n')
                contents = [line for line in contents if line.strip() != '']
                sent = '\n'.join(contents)
        else:
            f = request.urlopen(self.path).read()
            pages = pdf2image.convert_from_bytes(f, dpi=200, size=(1654, 2340))
            sent = pt.image_to_string(pages[0], lang='eng')
        return sent

    def doc_to_text(self):
        f = request.urlopen(self.path).read()
        fp = BytesIO(f)
        doc = Document(fp)
        contents = ''
        for paragraph in doc.paragraphs:
            contents = '\n'.join([contents, paragraph.text])
        return contents

    def load_skills(self, filename):
        f = open(filename, 'r')
        skills = []
        for line in f:
            skills.append(self.clean_phrase(line))
        f.close()
        return list(set(skills))

    def build_ngram_distribution(self, filename):
        n_s = [1, 2, 3]  # mono-, bi-, and tri-grams
        dist = {}
        for n in n_s:
            dist.update(self.parse_file(filename, n))
        return dist

    def parse_file(self, text, n):
        results = {}
        for line in text.split('\n'):
            words = self.clean_phrase(line).split(" ")
            words = [word.strip() for word in words]
            words = [word for word in words if word or not word.isdigit()]
            ngrams = self.ngrams(words, n)
            for tup in ngrams:
                phrase = " ".join(tup)
                if phrase in results.keys():
                    results[phrase] += 1
                else:
                    results[phrase] = 1
        return results

    def clean_phrase(self, line):
        return re.sub(r'[^\w\s]', '', line.replace('\n', '').replace('\t', '').lower())

    def ngrams(self, input_list, n):
        return list(zip(*[input_list[i:] for i in range(n)]))

    def measure1(self, v1, v2):
        return v1 - v2

    def measure2(self, v1, v2):
        return max(v1 - v2, 0)

    def measure3(self, v1, v2):  # cosine similarity
        # "compute cosine similarity of v1 to v2: (v1 dot v2)/{||v1||*||v2||)"
        sumxx, sumxy, sumyy = 0, 0, 0
        for i in range(len(v1)):
            x = v1[i]
            y = v2[i]
            sumxx += x * x
            sumyy += y * y
            sumxy += x * y
        if sumxx == 0 or sumyy == 0:
            return 0
        else:
            return sumxy / math.sqrt(sumxx * sumyy)

    def sendToFile(self):
        try:
            os.remove(self.outFile)
        except OSError:
            pass
        df = pd.DataFrame(self.table, columns=['type', 'skill', 'job', 'cv', 'm1', 'm2'])
        df_sorted = df.sort_values(by=['job', 'cv'], ascending=[False, False])
        df_sorted.to_csv(self.outFile, columns=['type', 'skill', 'job', 'cv'], index=False)

    def printMeasures(self):
        n_rows = len(self.table)
        v1 = [self.table[m1][4] for m1 in range(n_rows)]
        v2 = [self.table[m2][5] for m2 in range(n_rows)]
        print("Measure 1: ", str(sum(v1)))
        print("Measure 2: ", str(sum(v2)))
        similarities = []
        v1 = [self.table[jb][2] for jb in range(n_rows)]
        v2 = [self.table[cv][3] for cv in range(n_rows)]

        similarities.append(self.measure3(v1, v2))
        for type in ['hard', 'soft', 'general']:
            v1 = [self.table[jb][2] for jb in range(n_rows) if self.table[jb][0] == type]
            v2 = [self.table[cv][3] for cv in range(n_rows) if self.table[cv][0] == type]
            print(v1)
            print(v2)
            similarities.append(self.measure3(v1, v2))
        return similarities

    def makeTable(self):
        # I am interested in verbs, nouns, adverbs, and adjectives
        parts_of_speech = ['CD', 'JJ', 'JJR', 'JJS', 'MD', 'NN', 'NNS', 'NNP', 'NNPS', 'RB', 'RBR', 'RBS', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
        graylist = ["you", "will"]
        tmp_table = []
        tmp_table_two = []
        # look if the skills are mentioned in the job description and then in your cv

        for skill in self.hardskills:
            if skill in self.jb_distribution:
                count_jb = self.jb_distribution[skill]
                if skill in self.cv_distribution:
                    count_cv = self.cv_distribution[skill]
                else:
                    count_cv = 0
                m1 = self.measure1(count_jb, count_cv)
                m2 = self.measure2(count_jb, count_cv)
                tmp_table.append(['hard skill', skill, count_jb, count_cv, m1, m2])
                tmp_table_two.append(['hard skill', skill, count_jb, count_cv, m1, m2])

        for skill in self.softskills:
            if skill in self.jb_distribution:
                count_jb = self.jb_distribution[skill]
                if skill in self.cv_distribution:
                    count_cv = self.cv_distribution[skill]
                else:
                    count_cv = 0
                m1 = self.measure1(count_jb, count_cv)
                m2 = self.measure2(count_jb, count_cv)
                tmp_table.append(['soft skill', skill, count_jb, count_cv, m1, m2])
                tmp_table_two.append(['soft skill', skill, count_jb, count_cv, m1, m2])

        general_language = sorted(self.jb_distribution.items(), key=operator.itemgetter(1), reverse=True)
        for tuple in general_language:
            skill = tuple[0]
            if skill in self.hardskills or skill in self.softskills or skill in graylist:
                continue
            count_jb = tuple[1]
            tokens = nltk.word_tokenize(skill)
            parts = nltk.pos_tag(tokens)
            if all([parts[i][1] in parts_of_speech for i in range(len(parts))]):
                if skill in self.cv_distribution:
                    count_cv = self.cv_distribution[skill]
                else:
                    count_cv = 0
                m1 = self.measure1(count_jb, count_cv)
                m2 = self.measure2(count_jb, count_cv)
                tmp_table.append(['general', skill, count_jb, count_cv, m1, m2])
                if count_cv != 0:
                    tmp_table_two.append(['general', skill, count_jb, count_cv, m1, m2])
        self.table = tmp_table
        return tmp_table_two